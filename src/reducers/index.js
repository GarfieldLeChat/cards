import { combineReducers } from 'redux';
import CardsReducer from './reducer_cards';
import ActiveCard from './reducer_active_card';
import ClientsReducer from './reducer_clients';
import ActiveClient from './reducer_active_client';

const rootReducer = combineReducers({
  cards: CardsReducer,
  activeCard: ActiveCard,
  clients: ClientsReducer,
  activeClient: ActiveClient
});

export default rootReducer;