export default function() {
    return [
         { 
            ClientKey : "001",
            clientTitle : "Mr",
            clientFirstName : "Ollie",
            clientLastName : "Murphree",
            clientDOB : "01/07/1970",
            clientAnnualIncome : 34000,
            clientEmploymentStatus : "Full Time Employment",
            clientHouseNumber : 700,
            clientPostCode : "BS14 9PR",
            clientCardOptions : "Anywhere Card, Liquid Card"
        },
        { 
            ClientKey : "002",
            clientTitle : "Miss",
            clientFirstName : "Elizabeth",
            clientLastName : "Edmundson",
            clientDOB : "29/06/1984",
            clientAnnualIncome : 17000,
            clientEmploymentStatus : "Student",
            clientHouseNumber : 177,
            clientPostCode : "PH12 8UW",
            clientCardOptions : "Anywhere Card, Liquid Card, Student Card"
        },
        { 
            ClientKey : "003",
            clientTitle : "Mr",
            clientFirstName : "Trevor",
            clientLastName : "Rieck",
            clientDOB : "07/09/1990",
            clientAnnualIncome : 15000,
            clientEmploymentStatus : "Part Time Employed",
            clientHouseNumber : 343,
            clientPostCode : "TS25 2NF",
            clientCardOptions : "Anywhere Card"
        }
    ];
}