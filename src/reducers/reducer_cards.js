export default function() {
    return [
         { 
            CardKey : "001",
            cardName : "Student Life Card",
            cardDescription : "The Student Life credit card is only available to customers with an employment status of Student.",
            cardApr : "18.9",
            balanceTransferOfferDuration : 0,
            purchaseOfferDuration : 6,
            creditAvailable : 1200
        },
        { 
            CardKey : "002",
            cardName : "Anywhere Card",
            cardDescription : "The anywhere card is available to anyone anywhere.",
            cardApr : "33.9",
            balanceTransferOfferDuration : 0,
            purchaseOfferDuration : 0,
            creditAvailable : 300
        },
        { 
            CardKey : "003",
            cardName : "Liquid Card",
            cardDescription : "The Liquid card is a card available to customers who have an income of more than £16000.",
            cardApr : "33.9",
            balanceTransferOfferDuration : 12,
            purchaseOfferDuration : 6,
            creditAvailable : 300
        }
    ];
}