export function SelectCard(card) {
    return {
        type: 'CARD_SELECTED',
        payload: card
    };
}

export function SelectClient(client) {
    return {
        type: 'CLIENT_SELECTED',
        payload: client
    };
}