import React, { Component } from 'react';
import CardList from '../containers/card-list';
import CardDetail from '../containers/card-detail';
import ClientList from '../containers/client-list';
import ClientDetail from '../containers/client-detail';

export default class App extends Component {
  render() {
    return (
      <div>
        <div className='contianer'>
          <div className='row'>
          <h1>See details of cards and clients.</h1>
            <div className='col col-sm-12'>
                <ClientList />
            </div>
            <div className="col col-sm-12">
              <ClientDetail />
            </div>
            <div className='col col-sm-12'>
              <CardList />
            </div>
            <div className="col col-sm-12">
              <CardDetail />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
