import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SelectClient } from '../actions/index';
import { bindActionCreators } from 'redux';

class Clientlist extends Component {
    renderList() {
        return this.props.clients.map((client) =>{
            return (
                <li 
                    className='list-group-item'
                    key={client.ClientKey} 
                    onClick={() => this.props.SelectClient(client)}>
                    {client.clientTitle} {client.clientFirstName} {client.clientLastName}
                </li>
            );
        });
    }
    render() {
        return (
            <ul className="list-group col--4">
                {this.renderList()}
            </ul>
        )
    }
}

function mapStateToProps(state) {
    return {
        clients: state.clients
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators( { SelectClient: SelectClient}, dispatch )
}

export default connect(mapStateToProps, mapDispatchToProps)(Clientlist);