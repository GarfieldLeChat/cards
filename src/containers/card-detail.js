import React, { Component } from 'react';
import { connect } from 'react-redux';

class CardDetail extends Component {
    render() {
        if(!this.props.card) {
            return <div className='initial-text col-sm-12'>Select a card type to see more information on it. </div>
        }

        return (
            <div className='detail-panel cards'>
                <h3>Details of the Card</h3>
                <div>Card Name: {this.props.card.cardName}</div>
                <div>Card Description: {this.props.card.cardDescription}</div>
                <div>APR: {this.props.card.cardApr} %</div>
                <div>Balance Transfer Durtation: {this.props.card.balanceTransferOfferDuration} Months</div>
                <div>Card Purchase Offer Durtation: {this.props.card.purchaseOfferDuration} Months</div>
                <div>Available Credit: £{this.props.card.creditAvailable}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        card: state.activeCard
    };
}

export default connect(mapStateToProps)(CardDetail);