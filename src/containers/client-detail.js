import React, { Component } from 'react';
import { connect } from 'react-redux';

class ClientDetail extends Component {
    render() {
        if(!this.props.client) {
            return <div className='initial-text col-sm-12'>Select a client to see more information on them. </div>
        }

        return (
            <div className='detail-panel clients'>
                <h3>Details of the Client</h3>
                <div>Client Name: {this.props.client.clientTitle} {this.props.client.clientFirstName} {this.props.client.clientLastName}</div>
                <div>Client Date of Birth: {this.props.client.clientDOB}</div>
                <div>Client Annual Income: £ {this.props.client.clientAnnualIncome}</div>
                <div>Client Employment Status: {this.props.client.clientEmploymentStatus}</div>
                <div>Client House Number: {this.props.client.clientHouseNumber}</div>
                <div>Client Postcode: {this.props.client.clientPostCode}</div>
                <div>Client Card Options: {this.props.client.clientCardOptions}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        client: state.activeClient
    };
}

export default connect(mapStateToProps)(ClientDetail);