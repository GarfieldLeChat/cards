import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SelectCard } from '../actions/index';
import { bindActionCreators } from 'redux';

class Cardlist extends Component {
    renderList() {
        return this.props.cards.map((card) =>{
            return (
                <li 
                    className='list-group-item'
                    key={card.CardKey} 
                    onClick={() => this.props.SelectCard(card)}>
                    {card.cardName}
                </li>
            );
        });
    }
    render() {
        return (
            <ul className="list-group col--4">
                {this.renderList()}
            </ul>
        )
    }
}

function mapStateToProps(state) {
    return {
        cards: state.cards
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators( { SelectCard: SelectCard}, dispatch )
}

export default connect(mapStateToProps, mapDispatchToProps)(Cardlist);